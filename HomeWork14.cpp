﻿#include <iostream>
#include <ctime>
#include <Windows.h>


int main()
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	const int N = 4;
	int array[N][N];

	for (int i = 0; i < N; i++)
	{
		for(int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
		}
	}
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}
	
	
	int Day = st.wDay;
	int summ = 0;

	for (int i = 0; i < N; i++)
	{
		if (i == Day % N)
		{
			for (int j = 0; j < N; j++)
			{
				summ += array[i][j];
				std::cout << summ;
				std::cout << "\n";
			}
		}
	}

}